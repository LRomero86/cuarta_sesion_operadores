

/* Operador AND (&&) */

var a1 = true && true; console.log(a1)
var a2 = true && false; console.log(a2)
var a3 = false && true; console.log(a3)
var a4 = false && false; console.log(a4)

var a5 = false && (3==4);
console.log((3==4));

var a6 = "Cat" && "Dog";
console.log('El resultado del a6 es: ', a6);

var a7 = false && "Cat";
console.log("El resultado del a7 es: ", a7);
/******************************/

/* Operador OR (||) */

o1 = true || true; console.log(o1);
o2 = true || false; console.log(o2);
o3 = false || true; console.log(o3);
o4 = false || false; console.log(o4);

/*********************************/

o5 = "Cat" || "Dog"; console.log('El resultado es: ', o5);
o6 = false || "Dog"; console.log('El resultado es: ', o6);
o7 = "Cat" || false ; console.log('El resultado es: ', o7);
